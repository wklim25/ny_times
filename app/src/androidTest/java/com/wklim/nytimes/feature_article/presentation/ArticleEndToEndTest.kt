package com.wklim.nytimes.feature_article.presentation

import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import com.wklim.nytimes.MainActivity
import com.wklim.nytimes.Navigation
import com.wklim.nytimes.core.util.TestTags
import com.wklim.nytimes.feature_article.di.ArticleModule
import com.wklim.nytimes.ui.theme.NYTimesTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@UninstallModules(ArticleModule::class)
class ArticleEndToEndTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUp() {
        hiltRule.inject()
        composeRule.setContent {
            NYTimesTheme {
                Navigation()
            }
        }
    }

    @Test
    fun clickSearch_searchArticles_articleScreen() {
        composeRule.onNodeWithTag(TestTags.MAIN_SEARCH_BUTTON).performClick()
        composeRule.onNodeWithTag(TestTags.SEARCH_TEXT_FIELD).performTextInput("election")
        composeRule.onNodeWithTag(TestTags.SEARCH_BUTTON).performClick()
        composeRule.onNodeWithTag(TestTags.ARTICLE_LIST).assertExists()
    }

    @Test
    fun clickMostViewed_articleScreen() {
        composeRule.onNodeWithTag(TestTags.MOST_VIEWED_BUTTON).performClick()
        composeRule.onNodeWithTag(TestTags.ARTICLE_LIST).assertExists()
    }

    @Test
    fun clickMostShared_articleScreen() {
        composeRule.onNodeWithTag(TestTags.MOST_SHARED_BUTTON).performClick()
        composeRule.onNodeWithTag(TestTags.ARTICLE_LIST).assertExists()
    }

    @Test
    fun clickMostEmailed_articleScreen() {
        composeRule.onNodeWithTag(TestTags.MOST_EMAILED_BUTTON).performClick()
        composeRule.onNodeWithTag(TestTags.ARTICLE_LIST).assertExists()
    }
}
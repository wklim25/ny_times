package com.wklim.nytimes.feature_article.presentation.search_articles

import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.wklim.nytimes.MainActivity
import com.wklim.nytimes.core.util.TestTags
import com.wklim.nytimes.feature_article.di.ArticleModule
import com.wklim.nytimes.feature_article.presentation.util.Screens
import com.wklim.nytimes.ui.theme.NYTimesTheme
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

@HiltAndroidTest
@UninstallModules(ArticleModule::class)
class SearchArticlesScreenTest {

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val composeRule = createAndroidComposeRule<MainActivity>()

    @Before
    fun setUp() {
        hiltRule.inject()
        composeRule.setContent {
            val navController = rememberNavController()
            NYTimesTheme {
                NavHost(
                    navController = navController,
                    startDestination = Screens.SearchScreen.route
                ) {
                    composable(route = Screens.SearchScreen.route) {
                        SearchArticlesScreen(navController = navController)
                    }
                }
            }
        }
    }

    @Test
    fun searchComponents_exists() {
        composeRule.onNodeWithTag(TestTags.SEARCH_TEXT_FIELD).assertExists()
        composeRule.onNodeWithTag(TestTags.SEARCH_BUTTON).assertExists()
    }
}
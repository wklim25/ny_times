package com.wklim.nytimes.di

import android.app.Application
import androidx.room.Room
import com.wklim.nytimes.feature_article.data.local.ArticleDatabase
import com.wklim.nytimes.feature_article.data.remote.NYTimesApi
import com.wklim.nytimes.feature_article.data.repository.ArticleRepositoryImpl
import com.wklim.nytimes.feature_article.domain.repository.ArticleRepository
import com.wklim.nytimes.feature_article.domain.use_case.MostEmailedArticles
import com.wklim.nytimes.feature_article.domain.use_case.MostSharedArticles
import com.wklim.nytimes.feature_article.domain.use_case.MostViewedArticles
import com.wklim.nytimes.feature_article.domain.use_case.SearchArticles
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object TestArticleModule {

    @Provides
    @Singleton
    fun provideSearchArticlesUseCase(repository: ArticleRepository): SearchArticles {
        return SearchArticles(repository)
    }

    @Provides
    @Singleton
    fun provideMostViewedArticlesUseCase(repository: ArticleRepository): MostViewedArticles {
        return MostViewedArticles(repository)
    }

    @Provides
    @Singleton
    fun provideMostSharedArticlesUseCase(repository: ArticleRepository): MostSharedArticles {
        return MostSharedArticles(repository)
    }

    @Provides
    @Singleton
    fun provideMostEmailedArticlesUseCase(repository: ArticleRepository): MostEmailedArticles {
        return MostEmailedArticles(repository)
    }

    @Provides
    @Singleton
    fun provideArticleRepository(
        db: ArticleDatabase,
        api: NYTimesApi
    ): ArticleRepository {
        return ArticleRepositoryImpl(api, db.dao)
    }

    @Provides
    @Singleton
    fun provideArticleDatabase(app: Application): ArticleDatabase {
        return Room.inMemoryDatabaseBuilder(
            app, ArticleDatabase::class.java
        ).build()
    }

    @Provides
    @Singleton
    fun provideNYTimesApi(): NYTimesApi {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        val client: OkHttpClient = OkHttpClient.Builder().addInterceptor(interceptor).build()


        return Retrofit.Builder()
            .baseUrl(NYTimesApi.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NYTimesApi::class.java)
    }
}
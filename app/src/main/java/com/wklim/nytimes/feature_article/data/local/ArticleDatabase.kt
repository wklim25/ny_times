package com.wklim.nytimes.feature_article.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.wklim.nytimes.feature_article.data.local.entity.MostEmailedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostSharedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostViewedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.SearchArticleEntity

@Database(
    entities = [
        SearchArticleEntity::class,
        MostViewedArticleEntity::class,
        MostSharedArticleEntity::class,
        MostEmailedArticleEntity::class
    ],
    version = 1
)
abstract class ArticleDatabase : RoomDatabase() {
    abstract val dao: ArticleDao
}
package com.wklim.nytimes.feature_article.data.repository

import android.util.Log
import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.data.local.ArticleDao
import com.wklim.nytimes.feature_article.data.remote.NYTimesApi
import com.wklim.nytimes.feature_article.domain.model.Article
import com.wklim.nytimes.feature_article.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class ArticleRepositoryImpl(
    private val api: NYTimesApi,
    private val dao: ArticleDao
): ArticleRepository {

    override fun getSearchArticles(keyword: String): Flow<Resource<List<Article>>> = flow {
        emit(Resource.Loading())

        val articles = dao.getSearchArticles(keyword).map { it.toArticle() }
        emit(Resource.Loading(data = articles))

        try {
            val result = api.searchArticles(keyword = keyword)
            if (result.status == "OK") {
                val remoteArticles = result.response?.docs

                remoteArticles?.forEach { article ->
                    dao.deleteSearchArticle(article.uri)
                }

                remoteArticles?.let {
                    dao.insertSearchArticles(remoteArticles.map{
                        it.toSearchArticleEntity()
                    })
                }
            } else {
                emit(Resource.Error(
                    message = result.fault?.faultString ?: "Unknown result error",
                    data = articles
                ))
            }

        } catch (e: HttpException) {
            emit(Resource.Error(
                message = "Something went wrong!",
                data = articles
            ))
        } catch (e: IOException) {
            emit(Resource.Error(
                message = "Check internet connection.",
                data = articles
            ))
        }

        val newArticles = dao.getSearchArticles(keyword).map { it.toArticle() }
        emit(Resource.Success(data = newArticles))
    }

    override fun getMostViewedArticles(days: Int): Flow<Resource<List<Article>>> = flow {
        emit(Resource.Loading())

        val articles = dao.getMostViewedArticles().map { it.toArticle() }
        emit(Resource.Loading(data = articles))

        try {
            val result = api.popularViewedArticles(days)
            if (result.status == "OK") {
                val remoteArticles = result.results

                remoteArticles?.forEach { article ->
                    dao.deleteMostViewedArticles(article.uri)
                }

                remoteArticles?.let {
                    dao.insertMostViewedArticles(remoteArticles.map { it.toMostViewedArticleEntity() })
                }
            } else {
                emit(
                    Resource.Error(
                        message = result.fault?.faultString ?: "Unknown result error",
                        data = articles
                    )
                )
            }

        } catch (e: HttpException) {
            emit(
                Resource.Error(
                    message = "Something went wrong!",
                    data = articles
                )
            )
        } catch (e: IOException) {
            emit(
                Resource.Error(
                    message = "Check internet connection.",
                    data = articles
                )
            )
        }

        val newArticles = dao.getMostViewedArticles().map { it.toArticle() }
        emit(Resource.Success(data = newArticles))
    }

    override fun getMostSharedArticles(days: Int): Flow<Resource<List<Article>>> = flow {
        emit(Resource.Loading())

        val articles = dao.getMostSharedArticles().map { it.toArticle() }
        emit(Resource.Loading(data = articles))

        try {
            val result = api.popularSharedArticles(days)
            if (result.status == "OK") {
                val remoteArticles = result.results

                remoteArticles?.forEach { article ->
                    dao.deleteMostSharedArticles(article.uri)
                }

                remoteArticles?.let {
                    dao.insertMostSharedArticles(remoteArticles.map { it.toMostSharedArticleEntity() })
                }
            } else {
                emit(Resource.Error(
                    message = result.fault?.faultString ?: "Unknown result error",
                    data = articles
                ))
            }

        } catch (e: HttpException) {
            emit(Resource.Error(
                message = "Something went wrong!",
                data = articles
            ))
        } catch (e: IOException) {
            emit(Resource.Error(
                message = "Check internet connection.",
                data = articles
            ))
        }

        val newArticles = dao.getMostSharedArticles().map { it.toArticle() }
        emit(Resource.Success(data = newArticles))
    }

    override fun getMostEmailedArticles(days: Int): Flow<Resource<List<Article>>> = flow {
        emit(Resource.Loading())

        val articles = dao.getMostEmailedArticles().map { it.toArticle() }
        emit(Resource.Loading(data = articles))

        try {
            val result = api.popularEmailedArticles(days)
            if (result.status == "OK") {
                val remoteArticles = result.results

                remoteArticles?.forEach { article ->
                    dao.deleteMostEmailedArticles(article.uri)
                }

                remoteArticles?.let {
                    dao.insertMostEmailedArticles(remoteArticles.map { it.toMostEmailedArticleEntity() })
                }
            } else {
                emit(Resource.Error(
                    message = result.fault?.faultString ?: "Unknown result error",
                    data = articles
                ))
            }

        } catch (e: HttpException) {
            emit(Resource.Error(
                message = "Something went wrong!",
                data = articles
            ))
        } catch (e: IOException) {
            emit(Resource.Error(
                message = "Check internet connection.",
                data = articles
            ))
        }

        val newArticles = dao.getMostEmailedArticles().map { it.toArticle() }
        emit(Resource.Success(data = newArticles))
    }
}
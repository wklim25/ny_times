package com.wklim.nytimes.feature_article.presentation.util

sealed class Screens(val route: String) {
    object MainScreen : Screens("main_screen")
    object SearchScreen : Screens("search_screen")
    object ArticlesScreen: Screens("articles_screen")

    fun withArgs(arg: String): String {
        return buildString {
            append(route)
            append("?$arg")
        }
    }
}

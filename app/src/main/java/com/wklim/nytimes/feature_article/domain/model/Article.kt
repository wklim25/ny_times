package com.wklim.nytimes.feature_article.domain.model

import java.util.*

data class Article(
    val title: String,
    val image: String?,
    val publishDate: String,
    val url: String?,
    val uri: String
)

package com.wklim.nytimes.feature_article.data.remote.dto.search_article

data class BylineDto(
    val original: String
)
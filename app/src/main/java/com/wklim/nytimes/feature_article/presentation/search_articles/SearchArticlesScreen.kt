package com.wklim.nytimes.feature_article.presentation.search_articles

import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.wklim.nytimes.core.util.TestTags
import com.wklim.nytimes.feature_article.presentation.articles.InputType
import com.wklim.nytimes.feature_article.presentation.util.Screens
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SearchArticlesScreen(
    navController: NavController,
    viewModel: SearchArticlesViewModel = hiltViewModel()
) {
    val scaffoldState = rememberScaffoldState()

    LaunchedEffect(key1 = true) {
        viewModel.eventFlow.collectLatest { event ->
            when(event) {
                is SearchArticlesViewModel.UIEvent.ShowSnackbar -> {
                    scaffoldState.snackbarHostState.showSnackbar(
                        message = event.message
                    )
                }
                is SearchArticlesViewModel.UIEvent.SearchArticles -> {
                    navController.navigate(
                        Screens.ArticlesScreen
                        .withArgs("inputType=${InputType.Search.value}&searchText=${event.searchText}"))
                }
            }
        }
    }

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "Search",
                    style = MaterialTheme.typography.h4,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            TextField(
                value = viewModel.searchQuery.value,
                onValueChange = {
                    viewModel.onEvent(SearchArticlesEvent.EnteredKeyword(it))
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .testTag(TestTags.SEARCH_TEXT_FIELD),
                placeholder = {
                    Text(text = "Search articles here...")
                }
            )
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .testTag(TestTags.SEARCH_BUTTON),
                onClick = {
                    viewModel.onEvent(SearchArticlesEvent.SearchArticle)
                },
            ) {
                Text(text = "Search")
            }
        }
    }
}
package com.wklim.nytimes.feature_article.presentation.articles

import com.wklim.nytimes.feature_article.domain.model.Article

data class ArticlesState(
    val articles: List<Article> = emptyList(),
    val isLoading: Boolean = false
)
package com.wklim.nytimes.feature_article.presentation.search_articles

sealed class SearchArticlesEvent {
    data class EnteredKeyword(val value: String): SearchArticlesEvent()
    object SearchArticle: SearchArticlesEvent()
}
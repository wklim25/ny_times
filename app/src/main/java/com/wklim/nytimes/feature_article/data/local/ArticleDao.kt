package com.wklim.nytimes.feature_article.data.local

import androidx.room.*
import com.wklim.nytimes.feature_article.data.local.entity.MostEmailedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostSharedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostViewedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.SearchArticleEntity

@Dao
interface ArticleDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSearchArticles(docs: List<SearchArticleEntity>)

    @Query("DELETE FROM searcharticleentity WHERE uri = :uri")
    suspend fun deleteSearchArticle(uri: String)

    @Query("SELECT * FROM searcharticleentity WHERE keyword LIKE '%' || :keyword || '%'")
    suspend fun getSearchArticles(keyword: String): List<SearchArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMostViewedArticles(docs: List<MostViewedArticleEntity>)

    @Query("DELETE FROM mostviewedarticleentity WHERE uri = :uri")
    suspend fun deleteMostViewedArticles(uri: String)

    @Query("SELECT * FROM mostviewedarticleentity")
    suspend fun getMostViewedArticles(): List<MostViewedArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMostEmailedArticles(docs: List<MostEmailedArticleEntity>)

    @Query("DELETE FROM mostviewedarticleentity WHERE uri = :uri")
    suspend fun deleteMostEmailedArticles(uri: String)

    @Query("SELECT * FROM mostviewedarticleentity")
    suspend fun getMostEmailedArticles(): List<MostEmailedArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMostSharedArticles(docs: List<MostSharedArticleEntity>)

    @Query("DELETE FROM mostsharedarticleentity WHERE uri = :uri")
    suspend fun deleteMostSharedArticles(uri: String)

    @Query("SELECT * FROM mostsharedarticleentity")
    suspend fun getMostSharedArticles(): List<MostSharedArticleEntity>
}
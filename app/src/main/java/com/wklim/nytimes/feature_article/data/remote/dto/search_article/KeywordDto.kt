package com.wklim.nytimes.feature_article.data.remote.dto.search_article

data class KeywordDto(
    val value: String
)

package com.wklim.nytimes.feature_article.data.remote.dto.search_article

import com.wklim.nytimes.feature_article.data.local.entity.SearchArticleEntity

data class DocDto(
    val _id: String,
    val `abstract`: String,
    val byline: BylineDto,
    val document_type: String,
    val headline: HeadlineDto,
    val keywords: List<KeywordDto>,
    val lead_paragraph: String,
    val multimedia: List<MultimediaDto>,
    val news_desk: String,
    val print_page: String,
    val print_section: String,
    val pub_date: String,
    val section_name: String,
    val snippet: String,
    val source: String,
    val subsection_name: String,
    val type_of_material: String,
    val uri: String,
    val web_url: String,
    val word_count: Int
) {
    fun toSearchArticleEntity(): SearchArticleEntity {
        return SearchArticleEntity(
            title = headline.main,
            image = multimedia.find { media ->
                media.subType == "mediumThreeByTwo440"
            }?.let {
                "https://static01.nyt.com/" + it.url
            } ?: "",
            keyword = keywords.joinToString {
                it.value
            },
            publishDate = pub_date,
            url = web_url,
            uri = uri
        )
    }
}
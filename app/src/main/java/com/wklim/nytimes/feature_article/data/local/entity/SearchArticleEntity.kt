package com.wklim.nytimes.feature_article.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.wklim.nytimes.feature_article.domain.model.Article
import java.util.*

@Entity
data class SearchArticleEntity(
    val title: String,
    val image: String?,
    val keyword: String,
    val publishDate: String,
    val url: String?,
    val uri: String,
    @PrimaryKey val id: Int? = null
) {
    fun toArticle(): Article {
        return Article(
            title = title,
            image = image,
            publishDate = publishDate,
            url = url,
            uri = uri
        )
    }
}

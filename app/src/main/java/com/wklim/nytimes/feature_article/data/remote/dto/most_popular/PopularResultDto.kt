package com.wklim.nytimes.feature_article.data.remote.dto.most_popular

data class PopularResultDto(
    val copyright: String?,
    val num_results: Int?,
    val results: List<ResultDto>?,
    val status: String,
    val fault: FaultDto?
)
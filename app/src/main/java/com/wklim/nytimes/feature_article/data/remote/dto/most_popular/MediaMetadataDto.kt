package com.wklim.nytimes.feature_article.data.remote.dto.most_popular

data class MediaMetadataDto(
    val format: String,
    val height: Int,
    val url: String,
    val width: Int
)
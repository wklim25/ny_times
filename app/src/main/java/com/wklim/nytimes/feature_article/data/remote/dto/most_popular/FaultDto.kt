package com.wklim.nytimes.feature_article.data.remote.dto.most_popular

data class FaultDto(
    val faultString: String
)

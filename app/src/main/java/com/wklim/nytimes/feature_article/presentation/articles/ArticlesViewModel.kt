package com.wklim.nytimes.feature_article.presentation.articles

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.*
import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.domain.use_case.MostEmailedArticles
import com.wklim.nytimes.feature_article.domain.use_case.MostSharedArticles
import com.wklim.nytimes.feature_article.domain.use_case.MostViewedArticles
import com.wklim.nytimes.feature_article.domain.use_case.SearchArticles
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ArticlesViewModel @Inject constructor(
    private val searchArticle: SearchArticles,
    private val mostViewedArticles: MostViewedArticles,
    private val mostSharedArticles: MostSharedArticles,
    private val mostEmailedArticles: MostEmailedArticles,
    private val savedStateHandle: SavedStateHandle
) : ViewModel(), LifecycleObserver {
    private val _state = mutableStateOf(ArticlesState())
    val state: State<ArticlesState> = _state

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        refresh()
    }

    fun refresh() {
        when(InputType.fromInt(savedStateHandle.get<Int>("inputType") ?: 1)) {
            InputType.Search -> {
                search(savedStateHandle.get<String>("searchText") ?: "")
            }
            InputType.MostViewed -> {
                mostViewed()
            }
            InputType.MostShared -> {
                mostShared()
            }
            InputType.MostEmailed -> {
                mostEmailed()
            }
        }
    }

    private fun search(keyword: String) {
        viewModelScope.launch {
            searchArticle(keyword)
                .onEach { result ->
                    when(result) {
                        is Resource.Success -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                        }
                        is Resource.Error -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackbar(
                                    result.message ?: "Unknown Error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    private fun mostViewed() {
        viewModelScope.launch {
            mostViewedArticles()
                .onEach { result ->
                    when(result) {
                        is Resource.Success -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                        }
                        is Resource.Error -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackbar(
                                    result.message ?: "Unknown Error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    private fun mostShared() {
        viewModelScope.launch {
            mostSharedArticles()
                .onEach { result ->
                    when(result) {
                        is Resource.Success -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                        }
                        is Resource.Error -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackbar(
                                    result.message ?: "Unknown Error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    private fun mostEmailed() {
        viewModelScope.launch {
            mostEmailedArticles()
                .onEach { result ->
                    when(result) {
                        is Resource.Success -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                        }
                        is Resource.Error -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackbar(
                                    result.message ?: "Unknown Error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = _state.value.copy(
                                articles = result.data ?: emptyList(),
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    sealed class UIEvent {
        data class ShowSnackbar(val message: String): UIEvent()
    }
}
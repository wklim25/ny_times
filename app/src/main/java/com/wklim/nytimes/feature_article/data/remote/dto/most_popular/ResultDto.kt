package com.wklim.nytimes.feature_article.data.remote.dto.most_popular

import com.wklim.nytimes.feature_article.data.local.entity.MostEmailedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostSharedArticleEntity
import com.wklim.nytimes.feature_article.data.local.entity.MostViewedArticleEntity

data class ResultDto(
    val `abstract`: String,
    val adx_keywords: String,
    val asset_id: Long,
    val byline: String,
    val column: Any,
    val des_facet: List<String>,
    val eta_id: Int,
    val geo_facet: List<String>,
    val id: Long,
    val media: List<MediaDto>,
    val nytdsection: String,
    val org_facet: List<String>,
    val per_facet: List<String>,
    val published_date: String,
    val section: String,
    val source: String,
    val subsection: String,
    val title: String,
    val type: String,
    val updated: String,
    val uri: String,
    val url: String
) {
    fun toMostViewedArticleEntity(): MostViewedArticleEntity {
        return MostViewedArticleEntity(
            title = title,
            image = media.find { media ->
                media.type == "image"
            }?.`media-metadata`?.find { metaData ->
                metaData.format == "mediumThreeByTwo440"
            }?.url ?: "",
            publishDate = published_date,
            url = url,
            uri = uri
        )
    }
    fun toMostSharedArticleEntity(): MostSharedArticleEntity {
        return MostSharedArticleEntity(
            title = title,
            image = media.find { media ->
                media.type == "image"
            }?.`media-metadata`?.find { metaData ->
                metaData.format == "mediumThreeByTwo440"
            }?.url ?: "",
            publishDate = published_date,
            url = url,
            uri = uri
        )
    }
    fun toMostEmailedArticleEntity(): MostEmailedArticleEntity {
        return MostEmailedArticleEntity(
            title = title,
            image = media.find { media ->
                media.type == "image"
            }?.`media-metadata`?.find { metaData ->
                metaData.format == "mediumThreeByTwo440"
            }?.url ?: "",
            publishDate = published_date,
            url = url,
            uri = uri
        )
    }
}
package com.wklim.nytimes.feature_article.domain.repository

import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.domain.model.Article
import kotlinx.coroutines.flow.Flow

interface ArticleRepository {

    fun getSearchArticles(keyword: String): Flow<Resource<List<Article>>>

    fun getMostViewedArticles(days: Int): Flow<Resource<List<Article>>>

    fun getMostSharedArticles(days: Int): Flow<Resource<List<Article>>>

    fun getMostEmailedArticles(days: Int): Flow<Resource<List<Article>>>
}
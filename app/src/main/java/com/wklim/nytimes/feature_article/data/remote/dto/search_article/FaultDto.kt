package com.wklim.nytimes.feature_article.data.remote.dto.search_article

data class FaultDto(
    val faultString: String
)

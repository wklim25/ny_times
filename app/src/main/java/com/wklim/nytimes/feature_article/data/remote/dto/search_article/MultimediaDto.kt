package com.wklim.nytimes.feature_article.data.remote.dto.search_article

data class MultimediaDto(
    val crop_name: String,
    val height: Int,
    val rank: Int,
    val subType: String,
    val type: String,
    val url: String,
    val width: Int
)
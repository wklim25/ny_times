package com.wklim.nytimes.feature_article.domain.use_case

import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.domain.model.Article
import com.wklim.nytimes.feature_article.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow

class MostEmailedArticles(
    private val repository: ArticleRepository
) {
    operator fun invoke(days: Int = 30): Flow<Resource<List<Article>>> {
        return repository.getMostEmailedArticles(days)
    }
}
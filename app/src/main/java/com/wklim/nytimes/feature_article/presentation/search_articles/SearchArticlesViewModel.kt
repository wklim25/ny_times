package com.wklim.nytimes.feature_article.presentation.search_articles

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.wklim.nytimes.feature_article.presentation.articles.ArticlesViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchArticlesViewModel @Inject constructor() : ViewModel() {

    private val _searchQuery = mutableStateOf("")
    val searchQuery: State<String> = _searchQuery

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun onEvent(event: SearchArticlesEvent) {
        when(event) {
            is SearchArticlesEvent.EnteredKeyword -> {
                _searchQuery.value = event.value
            }
            is SearchArticlesEvent.SearchArticle -> {
                viewModelScope.launch {
                    if (_searchQuery.value.isBlank()) {
                        _eventFlow.emit(
                            UIEvent.ShowSnackbar(
                                message = "Please enter some text to search"
                            )
                        )
                    } else {
                        _eventFlow.emit(
                            UIEvent.SearchArticles(_searchQuery.value)
                        )
                    }
                }
            }
        }
    }

    sealed class UIEvent {
        data class ShowSnackbar(val message: String): UIEvent()
        data class SearchArticles(val searchText: String): UIEvent()
    }
}
package com.wklim.nytimes.feature_article.data.remote.dto.search_article

data class SearchResultDto(
    val copyright: String?,
    val response: ResponseDto?,
    val status: String?,
    val fault: FaultDto?
)
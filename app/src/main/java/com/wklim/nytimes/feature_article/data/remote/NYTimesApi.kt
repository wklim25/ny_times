package com.wklim.nytimes.feature_article.data.remote

import com.wklim.nytimes.BuildConfig.API_KEY
import com.wklim.nytimes.feature_article.data.remote.dto.most_popular.PopularResultDto
import com.wklim.nytimes.feature_article.data.remote.dto.search_article.SearchResultDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface NYTimesApi {
    @GET("/svc/search/v2/articlesearch.json")
    suspend fun searchArticles(
        @Query("q") keyword: String,
        @Query("api-key") apiKey: String = API_KEY
    ): SearchResultDto

    @GET("/svc/mostpopular/v2/viewed/{days}.json")
    suspend fun popularViewedArticles(
        @Path("days") days: Int = 30,
        @Query("api-key") apiKey: String = API_KEY
    ): PopularResultDto

    @GET("/svc/mostpopular/v2/shared/{days}.json")
    suspend fun popularSharedArticles(
        @Path("days") days: Int = 30,
        @Query("api-key") apiKey: String = API_KEY
    ): PopularResultDto

    @GET("/svc/mostpopular/v2/emailed/{days}.json")
    suspend fun popularEmailedArticles(
        @Path("days") days: Int = 30,
        @Query("api-key") apiKey: String = API_KEY
    ): PopularResultDto

    companion object {
        const val BASE_URL = "https://api.nytimes.com/"
    }
}
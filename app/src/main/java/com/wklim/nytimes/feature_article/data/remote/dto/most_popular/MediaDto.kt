package com.wklim.nytimes.feature_article.data.remote.dto.most_popular

data class MediaDto(
    val approved_for_syndication: Int,
    val caption: String,
    val copyright: String,
    val `media-metadata`: List<MediaMetadataDto>,
    val subtype: String,
    val type: String
)
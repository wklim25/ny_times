package com.wklim.nytimes.feature_article.domain.use_case

import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.domain.model.Article
import com.wklim.nytimes.feature_article.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SearchArticles(
    private val repository: ArticleRepository
) {
    operator fun invoke(keyword: String): Flow<Resource<List<Article>>> {
        if (keyword.isBlank()) {
            return flow{ }
        }

        return repository.getSearchArticles(keyword)
    }
}
package com.wklim.nytimes.feature_article.presentation.main

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.wklim.nytimes.core.util.TestTags
import com.wklim.nytimes.feature_article.presentation.articles.InputType
import com.wklim.nytimes.feature_article.presentation.util.Screens

@Composable
fun MainScreen(
    navController: NavController
) {
    Scaffold(
        topBar = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                Text(
                    text = "NYT",
                    style = MaterialTheme.typography.h4,
                    fontWeight = FontWeight.Bold
                )
            }
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = "Search",
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .testTag(TestTags.MAIN_SEARCH_BUTTON),
                onClick = {
                    navController.navigate(Screens.SearchScreen.route)
                },
            ) {
                Text(text = "Search Articles")
            }
            Spacer(modifier = Modifier.height(32.dp))
            Text(
                text = "Popular",
                modifier = Modifier.padding(vertical = 8.dp)
            )
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .testTag(TestTags.MOST_VIEWED_BUTTON),
                onClick = {
                    navController.navigate(Screens.ArticlesScreen
                        .withArgs("inputType=${InputType.MostViewed.value}"))
                },
            ) {
                Text(text = "Most Viewed")
            }
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .testTag(TestTags.MOST_SHARED_BUTTON),
                onClick = {
                    navController.navigate(Screens.ArticlesScreen
                        .withArgs("inputType=${InputType.MostShared.value}"))
                },
            ) {
                Text(text = "Most Shared")
            }
            Button(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 8.dp)
                    .testTag(TestTags.MOST_EMAILED_BUTTON),
                onClick = {
                    navController.navigate(Screens.ArticlesScreen
                        .withArgs("inputType=${InputType.MostEmailed.value}"))
                },
            ) {
                Text(text = "Most Emailed")
            }
        }
    }
}

@Preview
@Composable
fun MainScreenPreview() {
    MainScreen(navController = rememberNavController())
}
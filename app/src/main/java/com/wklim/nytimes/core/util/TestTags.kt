package com.wklim.nytimes.core.util

object TestTags {
    const val MAIN_SEARCH_BUTTON = "MAIN_SEARCH_BUTTON"
    const val MOST_VIEWED_BUTTON = "MOST_VIEWED_BUTTON"
    const val MOST_SHARED_BUTTON = "MOST_SHARED_BUTTON"
    const val MOST_EMAILED_BUTTON = "MOST_EMAILED_BUTTON"
    const val SEARCH_TEXT_FIELD = "SEARCH_TEXT_FIELD"
    const val SEARCH_BUTTON = "SEARCH_BUTTON"
    const val ARTICLE_LIST = "ARTICLE_LIST"
}
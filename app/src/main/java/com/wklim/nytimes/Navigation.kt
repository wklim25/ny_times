package com.wklim.nytimes

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.wklim.nytimes.feature_article.presentation.articles.ArticlesScreen
import com.wklim.nytimes.feature_article.presentation.articles.InputType
import com.wklim.nytimes.feature_article.presentation.main.MainScreen
import com.wklim.nytimes.feature_article.presentation.search_articles.SearchArticlesScreen
import com.wklim.nytimes.feature_article.presentation.util.Screens

@Composable
fun Navigation() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = Screens.MainScreen.route
    ) {
        composable(route = Screens.MainScreen.route) {
            MainScreen(navController = navController)
        }
        composable(route = Screens.SearchScreen.route) {
            SearchArticlesScreen(navController = navController)
        }
        composable(
            route = Screens.ArticlesScreen.route + "?inputType={inputType}&searchText={searchText}",
            arguments = listOf(
                navArgument("inputType") {
                    type = NavType.IntType
                    defaultValue = InputType.MostViewed.value
                },
                navArgument("searchText") {
                    type = NavType.StringType
                    defaultValue = ""
                }
            )
        ) { entry ->
            ArticlesScreen(
                navController = navController
            )
        }
    }
}
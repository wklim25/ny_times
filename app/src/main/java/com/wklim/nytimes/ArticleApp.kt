package com.wklim.nytimes

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ArticleApp: Application()
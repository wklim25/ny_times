package com.wklim.nytimes.feature_article.domain.use_case

import com.google.common.truth.Truth.assertThat
import com.wklim.nytimes.feature_article.data.repository.FakeArticleRepository
import com.wklim.nytimes.feature_article.domain.model.Article
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class SearchArticlesTest {

    private lateinit var searchArticles: SearchArticles
    private lateinit var fakeRepository: FakeArticleRepository

    @Before
    fun setUp() {
        fakeRepository = FakeArticleRepository()
        searchArticles = SearchArticles(
            repository = fakeRepository
        )
    }

    @Test
    fun `Search article, contains a`(): Unit = runBlocking{
        val articles = searchArticles("a").first()

        assertThat(articles.data?.size != null && articles.data!!.isNotEmpty()).isTrue()
    }
}
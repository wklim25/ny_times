package com.wklim.nytimes.feature_article.data.repository

import com.wklim.nytimes.core.util.Resource
import com.wklim.nytimes.feature_article.domain.model.Article
import com.wklim.nytimes.feature_article.domain.repository.ArticleRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class FakeArticleRepository : ArticleRepository {
    private val articles = mutableListOf<Article>()

    init {
        ('a'..'z').forEachIndexed { index, c ->
            articles.add(
                Article(
                    title = c.toString(),
                    image = "",
                    publishDate = "",
                    url = "",
                    uri = ""
                )
            )
        }
    }

    override fun getSearchArticles(keyword: String): Flow<Resource<List<Article>>> {
        return flow {
            emit(
                Resource.Success(
                    articles.toList().filter { article ->
                        article.title.contains(keyword)
                    }
                )
            )
        }
    }

    override fun getMostViewedArticles(days: Int): Flow<Resource<List<Article>>> {
        return flow { emit(Resource.Success(articles.toList())) }
    }

    override fun getMostSharedArticles(days: Int): Flow<Resource<List<Article>>> {
        return flow { emit(Resource.Success(articles.toList())) }
    }

    override fun getMostEmailedArticles(days: Int): Flow<Resource<List<Article>>> {
        return flow { emit(Resource.Success(articles.toList())) }
    }
}
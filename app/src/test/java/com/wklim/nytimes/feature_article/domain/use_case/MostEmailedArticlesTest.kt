package com.wklim.nytimes.feature_article.domain.use_case

import com.google.common.truth.Truth
import com.wklim.nytimes.feature_article.data.repository.FakeArticleRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

class MostEmailedArticlesTest {

    private lateinit var mostEmailedArticles: MostEmailedArticles
    private lateinit var fakeRepository: FakeArticleRepository

    @Before
    fun setUp() {
        fakeRepository = FakeArticleRepository()
        mostEmailedArticles = MostEmailedArticles(
            repository = fakeRepository
        )
    }

    @Test
    fun `Most emailed article, contains articles`(): Unit = runBlocking{
        val articles = mostEmailedArticles().first()

        Truth.assertThat(articles.data?.size != null && articles.data!!.isNotEmpty()).isTrue()
    }
}
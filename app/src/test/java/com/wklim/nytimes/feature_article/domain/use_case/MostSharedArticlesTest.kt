package com.wklim.nytimes.feature_article.domain.use_case

import com.google.common.truth.Truth
import com.wklim.nytimes.feature_article.data.repository.FakeArticleRepository
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class MostSharedArticlesTest {

    private lateinit var mostSharedArticles: MostSharedArticles
    private lateinit var fakeRepository: FakeArticleRepository

    @Before
    fun setUp() {
        fakeRepository = FakeArticleRepository()
        mostSharedArticles = MostSharedArticles(
            repository = fakeRepository
        )
    }

    @Test
    fun `Most shared article, contains articles`(): Unit = runBlocking{
        val articles = mostSharedArticles().first()

        Truth.assertThat(articles.data?.size != null && articles.data!!.isNotEmpty()).isTrue()
    }
}